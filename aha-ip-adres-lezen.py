line = "192.168.0.1"
parts = line.split('.')

# unconverted
print(parts)

# convert to int
for i in range(0, len(parts)):
    parts[i] = int(parts[i])

# output    
print(parts)
