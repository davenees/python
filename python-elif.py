# decl variabelen
byte1 = int(172)
byte2 = int(16)
byte3 = int(15)
byte4 = int(15)

# checken met if, elif, else
# volledig ip adres checken
if byte1 < 0 or byte1 > 255 or byte2 < 0 or byte2 > 255 or byte3 < 0 or byte3 >255 or byte4 < 0 or byte4 >255:
    print("byte 1 tot byte 4 stellen samen geen geldig IPv4-adres voor!")
# private range 192.168.0.0 - 192.168.255.255
elif byte1 == 192 and byte2 == 168:
    print("byte 1 tot byte 4 stellen samen een geldig IPv4-adres in het privébereik voor")
# private range 172.16.0.0 - 172.31.255.255
elif byte1 == 172 and byte2 >= 16 and byte2 <= 31:
    print("byte 1 tot byte 4 stellen samen een geldig IPv4-adres in het privébereik voor")
# private range 10.0.0.0 - 10.255.255.255
elif byte1 == 10:
    print("byte 1 tot byte 4 stellen samen een geldig IPv4-adres in het privébereik voor")
# in alle andere gevallen geldt:
else:
    print("byte 1 tot byte 4 stellen samen een geldig IPv4-adres in het publiek bereik voor")
