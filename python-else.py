# declaratie variabelen
byte1 = int(10)
byte2 = int(15)
byte3 = int(256)
byte4 = int(1)

# controle geldig IPv4-adres
if byte1 < 0 or byte1 > 255 and byte1 != 10:
    print("De vier bytes vormen geen geldig IPv4-adres!")
if byte2 < 0 or byte2 > 255 and byte1 != 10:
    print("De vier bytes vormen geen geldig IPv4-adres!")
if byte3 < 0 or byte3 > 255 and byte1 != 10:
    print("De vier bytes vormen geen geldig IPv4-adres!")
if byte4 < 0 or byte4 > 255 and byte1 != 10:
    print("De vier bytes vormen geen geldig IPv4-adres!")

# controle range 10.0.0.0/8
if byte1 != 10:
    print("De vier bytes vormen een IPv4-adres buiten het bereik 10.0.0.0/8")
else:
    print("De vier bytes vormen een IPv4-adres binnen het bereik 10.0.0.0/8")
