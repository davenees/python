seconden_per_minuut = 60
minuten_per_uur = 60
uren_per_dag = 24
dagen_per_week = 7

seconden_per_week = (dagen_per_week * uren_per_dag) * (minuten_per_uur * seconden_per_minuut)

boodschap = "Er zijn zo veel seconden in een week: " + str(seconden_per_week)

print(boodschap)

